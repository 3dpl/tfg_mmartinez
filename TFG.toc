\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\contentsline {chapter}{Abstract}{v}{chapter*.2}
\contentsline {chapter}{Contents}{vii}{chapter*.3}
\contentsline {chapter}{List of Figures}{ix}{chapter*.4}
\contentsline {chapter}{List of Tables}{xi}{chapter*.5}
\contentsline {chapter}{List of acronyms}{xv}{chapter*.7}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.8}
\contentsline {section}{\numberline {1.1}Outline}{1}{section.9}
\contentsline {section}{\numberline {1.2}Motivation}{1}{section.10}
\contentsline {section}{\numberline {1.3}Related works}{2}{section.11}
\contentsline {subsection}{\numberline {1.3.1}Segmentation in general}{2}{subsection.12}
\contentsline {subsection}{\numberline {1.3.2}Segmentation with Convolutional Neural Networks}{2}{subsection.13}
\contentsline {subsection}{\numberline {1.3.3}People segmentation}{2}{subsection.14}
\contentsline {subsection}{\numberline {1.3.4}why convolutional for images}{2}{subsection.15}
\contentsline {section}{\numberline {1.4}Datasets comparison}{2}{section.16}
\contentsline {chapter}{\numberline {2}Convolutional Neural Networks}{3}{chapter.17}
\contentsline {section}{\numberline {2.1}Introduction}{3}{section.18}
\contentsline {section}{\numberline {2.2}\acs {CNN} architecture}{3}{section.21}
\contentsline {subsection}{\numberline {2.2.1}Convolution}{3}{subsection.22}
\contentsline {subsubsection}{Local Receptive Fields}{4}{section*.23}
\contentsline {subsubsection}{Shared weights}{4}{section*.24}
\contentsline {subsection}{\numberline {2.2.2}Activation}{4}{subsection.25}
\contentsline {subsection}{\numberline {2.2.3}Pooling}{5}{subsection.26}
\contentsline {chapter}{\numberline {3}Methodology}{7}{chapter.27}
\contentsline {section}{\numberline {3.1}Libraries and frameworks}{7}{section.28}
\contentsline {subsection}{\numberline {3.1.1}CUDA}{7}{subsection.29}
\contentsline {subsubsection}{cuDNN}{9}{section*.32}
\contentsline {subsection}{\numberline {3.1.2}Tensorflow}{9}{subsection.33}
\contentsline {subsection}{\numberline {3.1.3}Keras}{10}{subsection.38}
\contentsline {subsubsection}{Optimizer}{11}{section*.53}
\contentsline {subsubsection}{Loss function}{12}{section*.62}
\contentsline {subsubsection}{Metrics}{12}{section*.63}
\contentsline {subsubsection}{Compiling example}{12}{section*.64}
\contentsline {subsubsection}{Fit}{13}{section*.70}
\contentsline {subsubsection}{Callbacks}{14}{section*.79}
\contentsline {subsubsection}{Example of fit and callbacks}{15}{section*.85}
\contentsline {subsection}{\numberline {3.1.4}Other packages}{16}{subsection.120}
\contentsline {section}{\numberline {3.2}Technologies}{16}{section.132}
\contentsline {section}{\numberline {3.3}Development software environment}{16}{section.133}
\contentsline {subsection}{\numberline {3.3.1}First stages of development environment}{17}{subsection.134}
\contentsline {subsection}{\numberline {3.3.2}Training software environment}{17}{subsection.136}
\contentsline {subsection}{\numberline {3.3.3}Miscellaneous}{18}{subsection.138}
\contentsline {section}{\numberline {3.4}Hardware}{20}{section.140}
\contentsline {chapter}{\numberline {4}People segmentation with U-Net}{23}{chapter.148}
\contentsline {section}{\numberline {4.1}Introduction}{24}{section.149}
\contentsline {section}{\numberline {4.2}Dataset preprocessing}{24}{section.150}
\contentsline {section}{\numberline {4.3}U-Net}{25}{section.152}
\contentsline {section}{\numberline {4.4}data augmentation}{25}{section.154}
\contentsline {section}{\numberline {4.5}Layers}{26}{section.155}
\contentsline {subsection}{\numberline {4.5.1}Convolution}{26}{subsection.168}
\contentsline {subsection}{\numberline {4.5.2}Batch normalization}{26}{subsection.169}
\contentsline {subsection}{\numberline {4.5.3}Activation}{26}{subsection.170}
\contentsline {subsection}{\numberline {4.5.4}Pooling}{26}{subsection.171}
\contentsline {subsection}{\numberline {4.5.5}Upsampling}{26}{subsection.172}
\contentsline {subsection}{\numberline {4.5.6}Concatenation}{26}{subsection.173}
\contentsline {section}{\numberline {4.6}Models}{26}{section.174}
\contentsline {section}{\numberline {4.7}Loss functions}{26}{section.175}
\contentsline {chapter}{\numberline {A}U - NET source code}{27}{appendix.176}
\contentsline {chapter}{\numberline {B}Packages used}{29}{appendix.177}
\contentsline {chapter}{\numberline {C}Concepts}{31}{appendix.178}
\contentsline {section}{\numberline {C.1}Momentum}{31}{section.179}
\contentsline {section}{\numberline {C.2}Nesterov accelerated gradient}{31}{section.181}
\contentsline {section}{\numberline {C.3}Learning rate}{32}{section.183}
\contentsline {chapter}{Bibliography}{35}{appendix*.185}
